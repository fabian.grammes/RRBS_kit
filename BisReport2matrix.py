"""Merge bismark reports."""

import os
import sys
import argparse

# ------------------------------------------------------------------------------
# create pareser object
parser = argparse.ArgumentParser(description = 'Merge multiple fastq files into one')

# add command line options
parser.add_argument('-d', '--dir', type=str,
                    help='Folder where sorted bismark coverage reports with name tag are stored')
parser.add_argument('-e', '--ending', type=str, default = ".bismark.cov.sort", 
                    help='File ending for sorted bismark coverage reports with name tag. Defaults to ".bismark.cov.sort"')

# read the command line inputs
args = parser.parse_args()
# args = parser.parse_args(['-d','test',
#                           '-e', "_R1.cutadapt.fq_trimmed_bismark_SE_report.txt"])

# ------------------------------------------------------------------------------

## identify all files that will be joined
fl = [f for f in os.listdir(args.dir) if f.endswith(args.ending)]
fn = [os.path.join(args.dir, f) for f in fl]
files = dict(zip([f.replace(args.ending, "") for f in fl], fn))

rep_matrix = dict()
for sample, rep_file in files.items():
    with open(rep_file) as bis:
        count=0
        rep = []
        for line in bis:
            lp = line.strip().split("\t")
            count+=1
            if count in [7,8,10,11,26,30]:
                rep.append(lp[1])
        rep_matrix[sample] = rep

with open(os.path.join(args.dir, "Bismark_report.matrix"),"w") as out_matr:
    cols = ["sample","total","unique","nohit","ambigious","mCpG","CpG"]
    out_matr.write("%s\n" % "\t".join(cols))
    for r in rep_matrix:
        out_matr.write("%s\t%s\n" % (r, "\t".join(rep_matrix[r])))
        
