"""Merge methylation files."""

import os
import sys
import argparse
import pandas as pd


# ------------------------------------------------------------------------------
# create pareser object
parser = argparse.ArgumentParser(description = 'Merge multiple bismark coverage reports files into one')

# add command line options
parser.add_argument('-d', '--dir', type=str,
                    help='Folder where sorted bismark coverage reports with name tag are stored')

parser.add_argument('-e', '--ending', type=str, default = ".bismark.cov.sort", 
                    help='File ending for sorted bismark coverage reports with name tag. Defaults to ".bismark.cov.sort"')

parser.add_argument('-o', '--output', type=str, default = "bismark.coverage", 
                    help='Output prefix. 2 Files are generated: <prefix>.matrix.txt; <prefix>.samples.txt')


# read the command line inputs
args = parser.parse_args()

# for testing
# args = parser.parse_args(['-d','test',
#                            '-e', ".cov.sort.n.gz",
#                            '-o', "test/lice_infected_rrbs",
#                           '--na_filter', '1'])

# ------------------------------------------------------------------------------
## identify all files that will be joined
fl = [f for f in os.listdir(args.dir) if f.endswith(args.ending)]
fn = [os.path.join(args.dir, f) for f in fl]
files = dict(zip([f.replace(args.ending, "") for f in fl], fn))

## write sample file
with open(args.output+".samples.txt", "w") as sample_handle:
    c=0
    sample_handle.write("%s\t%s\n" % ("sample", "index"))
    for k in files.keys():
        c+=1
        sample_handle.write("%s\t%s\n" % (k, c))
        
## Read the first coverage repport to data frame 
df1 = pd.read_table(files.values()[0], header=None, usecols=[0, 1, 4, 5],
                    compression="gzip").set_index([0, 1])
df1.columns = ["numCs1", "numTs1"]
df1['coverage1'] = df1["numCs1"]+df1["numTs1"]
df1 = df1[['coverage1', "numCs1", "numTs1"]]
sys.stdout.write("- Read -- %s\n" % (files.values()[0]))

## Merge all remaining cov report onto df1
counter = 1
for f in files.values()[1:]:
    columns = [c+str(counter) for c in ["numCs","numTs","coverage"]]
    counter+=1
    dfr = pd.read_table(f, header=None, usecols = [0,1,4,5],
                        compression = "gzip").set_index([0,1])
    dfr.columns = ["numCs","numTs"]
    dfr['coverage'] = dfr["numCs"]+dfr["numTs"]
    dfr = dfr[['coverage',"numCs","numTs"]]
    dfr.columns = [c+str(counter) for c in ["coverage","numCs","numTs"]]
    df1 = pd.concat([df1, dfr], axis=1, join="outer")
    sys.stdout.write("- Merged %i -- %s -- %s Rows\n" % (counter, f, len(dfr)))


    
# ## Filter by coverage
# df1_cov = df1.filter(regex="coverage")
# df1_0s = df1_cov.isnull().sum(axis=1)
# df1_mcov = df1_cov.mean(axis=1)

# naf = round(len(df1_cov.columns) * args.na_filter)

# dff = df1[(df1_0s < naf) & (df1_mcov > args.mean_filter)]
# ## replace NAs
# dff = dff.fillna(-1).astype(int)
# dff = dff.replace(-1, "NA")

# ## make methyKit conform
strand = ["+"] * len(df1)
df1 = df1.set_index([df1.index.get_level_values(1), strand], append=True)

sys.stdout.write("""
N rows (no filtering): %i 
""" % (len(df1)))

df1.to_csv(args.output+".matrix.txt.gz", sep="\t", chunksize=100000,
           compression="gzip", na_rep="NaN")

