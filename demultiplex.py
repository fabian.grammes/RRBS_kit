
from itertools import izip
from multiprocessing import Pool
import collections
import HTSeq
import gzip
import os
import sys
import argparse


# ------------------------------------------------------------------------------
# create pareser object
parser = argparse.ArgumentParser(description = 'Script to demultiplex RRBS reads')

# add command line options
parser.add_argument('-i', '--index', type=str,
                    help='Index file in .fastq format')
parser.add_argument('-f', '--fastq', type=str,
                    help='Read file .fastq format')
parser.add_argument('-m', '--master', type=str,
                    help='Read the master file containaing sample-index information')
parser.add_argument('--keep_unknown', action='store_true',
                    help='Keep unknown reads')
parser.add_argument('-b', '--bases', type=int, default = 5,
                    help='Number index bases to be used for sample matching. Default is the first 5 bases')
parser.add_argument('-o', '--outfolder', type=str, default = './',
                    help='Path to the folder were the demultiplexed files are stored')
parser.add_argument('-c', '--chunk', type=int, default = 20000,
                    help='Chunk size')


# read the command line inputs
args = parser.parse_args()

# for testing
# args = parser.parse_args(['-i', 'test/test_I1.fastq.gz',
#                           '-f', 'test/test_R1.fastq.gz',
#                           '-m', 'test/test_pool1.txt',
#                           '-o', 'testout',
#                           '-b', '6'])


sys.stderr.write('---------------------------------------------------------------------\n')
sys.stderr.write('demultiplex.py v001\n')
for arg in vars(args):
    sys.stderr.write("%s\t%s\n" % (arg, getattr(args, arg)))
sys.stderr.write('---------------------------------------------------------------------\n')

# ------------------------------------------------------------------------------

out_folder = args.outfolder
if not out_folder.endswith("/"):
    out_folder = out_folder+"/"
    # if the out folder already exists d
if not os.path.exists(out_folder):
    os.makedirs(out_folder)

# read the master
with open(args.master, "r") as ins:
    master = dict()
    for line in ins:
        if line.startswith("Sample"):
            continue
        line = line.strip().split("\t")
        idx = line[1][:args.bases]
        if idx in master:
            sys.exit("The Computer sais NO! Index sequence exists more than once!!")
        master[line[1][:args.bases]] = line[0]

def write_loop(dic, folder, name):
    for d in dic:
        with gzip.open(folder+d+name, "a") as fi:
            for dd in dic[d]:
                fi.write("@%s\n%s\n+\n%s\n" % (dd.name, dd.seq, dd.qualstr))
                

# @profile
def loop_fastq(index_fq, read_fq, master, out_folder):
    index = collections.defaultdict(int)
    d_ind = collections.defaultdict(list)
    d_read = collections.defaultdict(list)
    nreads = 0
    for i, f in izip(HTSeq.FastqReader(index_fq),
                     HTSeq.FastqReader(read_fq)):
        nreads+=1
        ind = i.seq[:args.bases]
        # Reading
        if ind not in master.keys():
            index['_unknown'] += 1
            continue
        if ind in master.keys():
            index[ind] += 1
            samp = master[ind]
            d_read[samp].append(f)
            d_ind[samp].append(i)

        # Writing: After reading through 10.000 sequences write to file
        if nreads % args.chunk == 0:
            sys.stderr.write( "%d reads processed.\n" % nreads)
            write_loop(d_read, out_folder, "_R1.fastq.gz")
            write_loop(d_ind, out_folder, "_I1.fastq.gz")
            # reset dictionaries
            d_ind = collections.defaultdict(list)
            d_read = collections.defaultdict(list)
    # write out legftovers at the end
    write_loop(d_read, out_folder, "_R1.fastq.gz")
    write_loop(d_ind, out_folder, "_I1.fastq.gz")
            
    return(index)

index = loop_fastq(args.index, args.fastq, master, out_folder)
index = collections.OrderedDict(sorted(index.items()))


with open(out_folder+"Summary.txt", "w") as summary:
    total = float(sum(index.values()))
    summary.write("%s\t%s\t%s\t%s\n" % ("Sample", "Index", "Count", "Percent"))
    for i in index:
        if i != "_unknown":
            sample = master[i]
        else:
            sample = "NA"
        summary.write("%s\t%s\t%d\t%f\n" % (sample, i, index[i], (index[i]/total)*100))









