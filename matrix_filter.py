"""Filter methylation matrix"""
import os
import sys
import argparse
import pandas as pd

# ------------------------------------------------------------------------------
# create pareser object
parser = argparse.ArgumentParser(description = 'Filter a bismark coverage report matrix')

parser.add_argument('-m', '--matrix', type=str,
                    help="Path to .matrix.txt.gz file")
parser.add_argument('-n', '--na_filter', type=float, default = 0.1, 
                    help="Filter: Remove rows containing more than x percent of missing values. Default is 0.1; 10 missing values")
parser.add_argument('-f', '--mean_filter', type=float, default = 1.0, 
                    help="Filter: Retain only rows with a mean coverage higher than filter. Default is 1")
parser.add_argument('--snps', type=str, 
                    help="SNPs file containing C/T SNPs to exclude: 2 columns: chrom, position")
parser.add_argument('--min', type=int, default = 1, 
                    help="Minimum coverage to accept a positions. Rows containing more than x percent of values lower than the min are removed")

# read the command line inputs
args = parser.parse_args()
for arg in vars(args):
    sys.stdout.write("%s = %s\n" % (arg, getattr(args, arg)))
    
# args = parser.parse_args(['-m', 'lice_rrbs.matrix.txt.gz',
#                            '--na_filter', '0.1',
#                            '--mean_filter', '5',
#                           '--snps',"../Aquagenome_CT-SNPs.txt",
#                           '--min','2'])



unfilt = pd.read_table(args.matrix, header=0, compression="gzip")
unfilt = unfilt.drop(unfilt.columns[[2, 3]], axis =1)
unfilt = unfilt.set_index(['0', '1'])

## Filter by coverage
df1_cov = unfilt.filter(regex="coverage")
naf = round(len(df1_cov.columns) * args.na_filter)
df1_0s = df1_cov.isnull().sum(axis=1)
df1_mcov = df1_cov.mean(axis=1)
df1_min = (df1_cov < args.min).sum(axis = 1)

filterd_1 = unfilt[(df1_0s < naf) & (df1_mcov > args.mean_filter) & (df1_min < naf)]

## Filter out SNPs
cpg = filterd_1.reset_index()[['0','1']]
cpg.columns = ["chrom","start"]
cpg["A"] = range(len(cpg)) 
snp = pd.read_table(args.snps, header=None)[[0,1]]
snp.columns = ["chrom","start"]

bad = pd.merge(cpg, snp, on=['chrom','start'], how='inner')['A'].tolist()

filterd_2 = filterd_1.drop(filterd_1.index[bad])

## replace NAs
strand = ["+"] * len(filterd_2)
filterd_2 = filterd_2.set_index([filterd_2.index.get_level_values(1), strand], append=True)
filterd_2 = filterd_2.fillna(-1).astype(int)
filterd_2 = filterd_2.replace(-1, "NA")

sys.stdout.write("""
Filtering: Tollerating NAs < %s and a mean coverage > %s per row
N rows (original): %i 
N rows (filtered out by mean): %i 
N rows (filtered out by NAs): %i
N rows (filtered out by minimum count): %i
N rows (filtered out by overlapping with C/T polymorphism): %i
N rows Final: %i

""" % (str(naf), str(args.mean_filter), len(unfilt),
       sum(df1_0s < naf), sum(df1_mcov > args.mean_filter), sum(df1_min > naf), len(bad), len(filterd_2)))

out_file = args.matrix.replace(".txt.gz",".filterd.txt.gz")

filterd_2.to_csv(out_file, sep="\t", chunksize = 100000,
           header=False, compression="gzip")


