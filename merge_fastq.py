import os, collections, gzip, argparse, sys, io
import shutil

# create pareser object
parser = argparse.ArgumentParser(description = 'Merge multiple fastq files into one')

# add command line options
parser.add_argument('-f', '--fq_folder', type=str,
                    help='Folder where the subfolders with .fastq.gz files are stored')

parser.add_argument('--ending', type=str, default = 'fastq.gz',
                    help='File ending of the files that will be merged')

parser.add_argument('-o', '--outfolder', type=str, default = './',
                    help='Path to the folder were the demultiplexed files are stored')

parser.add_argument('--clean', action='store_true',
                    help='Delete the .fastq files after they have been merged')

# read the command line inputs
args = parser.parse_args()

# for testing
# args = parser.parse_args(['-f','tt','-o','test_join'])


sys.stderr.write('---------------------------------------------------------------------\n')
sys.stderr.write('merge_fastq.py v001\n')
for arg in vars(args):
    sys.stderr.write("%s\t%s\n" % (arg, getattr(args, arg)))
sys.stderr.write('---------------------------------------------------------------------\n')
#-------------------------------------------------------------------------------

# Collect all .fastq.gz files
sums = list()
fq_dict = collections.defaultdict(list)
for (dirpath, dirnames, filenames) in os.walk(args.fq_folder):
    for fn in filenames: 
        if fn.endswith('.fastq.gz'):
            fq_dict[fn].append(dirpath)
        elif fn == 'Summary.txt':
            sums.append(os.path.join(dirpath, fn))
            

# Merge summaries
counts = collections.defaultdict(int)
indices = dict()
for s in sums:
    with open(s, 'r') as in_sum:
        for ss in in_sum:
            if ss.startswith('Sample'):
                continue
            line = ss.strip().split('\t')
            counts[line[0]] += int(line[2])
            indices[line[0]] = line[1]

# and write summary to file
ocounts = collections.OrderedDict(sorted(counts.items()))
with open(os.path.join(args.outfolder,"Summary.txt"), "w") as summary:
    total = float(sum(ocounts.values()))
    summary.write("%s\t%s\t%s\t%s\n" % ("Sample", "Index", "Count", "Percent"))
    for c in ocounts:
        if c != "NA":
            index = indices[c]
        else:
            index = "_unknown"
        summary.write("%s\t%s\t%d\t%f\n" % (c, index, ocounts[c], (ocounts[c]/total)*100))
                        
# Merge all .fastq.gz files and write 2 file
# @profile
def gz_merge(fastq_dict, out_folder):
    for fq in fastq_dict:
        fq_list = [os.path.join(i, fq) for i in fastq_dict[fq]]
        for fq_name in fq_list:
            with open(os.path.join(out_folder, fq), 'ab') as outfile:
                with open(fq_name, 'rb') as infile:
                    shutil.copyfileobj(infile, outfile)
            if args.clean:
                os.remove(fq_name)
                sys.stderr.write( "Removed: %s\n" % fq_name)
                        

gz_merge(fq_dict, args.outfolder)
                
