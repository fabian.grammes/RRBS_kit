# Relate CpGs to RRBS methylome
# Experimental

import sys
import re
import HTSeq
import argparse
from collections import OrderedDict, defaultdict, Counter

# ------------------------------------------------------------------------------
# create pareser object
parser = argparse.ArgumentParser(description = 'Experimental', formatter_class=argparse.RawTextHelpFormatter)

# add command line options
parser.add_argument('-i', '--input', type=str,
                    help="Input file tab delimited with 3 columns: chrom, pos, mean_coverage")

parser.add_argument('-f', '--fasta', type=str,  
                    help='Path to the .fasta genopme file that has been used with bismark')

parser.add_argument('-o', '--output', type=str, default = "rrbs_methylome", ≈
                    help="Output prefix. 2 Files are generated:\n"
                    "<prefix>.insilico.no_feature.bed        - Contains CpG locations that are not possible according\n"
                    "                                          to the insilico RRBS methylome\n"
                    "<prefix>.insilico.methylome_summary.txt - Tab delimited file contains methylome fragments\n"
                    "                                          that were linked to the CpGs from the input file\ni "
                    "<prefix>.insilico.methylome.bed         - Contains all methylome fragments")
parser.add_argument('--read_length', type=int, default = 101, 
                    help="Read length")


# read the command line inputs
args = parser.parse_args()
≈
# for testing
# args = parser.parse_args(['-i','meth_sorted/lice_rrbs.matrix.filterd.mean_cov.txt',
#                           '-f', "/mnt/users/fabig/Ssa_genome/bismark_bowtie1_ChrOnly/index/Salmon_genome.chr.fa",
#                           '-o', "meth_sorted/lice_rrbs.matrix.filterd"])

read_len = args.read_length

## 1st Read the genome straight from fasta file
# - Identify Msp1 restriction sites
# - Identfy fragments
# - Set left/right intervals
rrbs_methylome = HTSeq.GenomicArrayOfSets("auto", stranded=False)

with open(args.output+".insilico.methylome.bed", "w") as out_handle:≈≈≈
    for chrom in HTSeq.FastaReader(args.fasta):
        sys.stdout.write("Reading: %s\n" %chrom.name)
        chrom_len = len(chrom.seq)
        ## find CpGs
        frag_list = []
        phit = 0
        for m in re.finditer("CCGG", str(chrom.seq)):
            hit = m.start(0)+1 # these are 0 based coordinates which are HTseq conform
            frag_size = hit-phit
            frag_id = "%s:%i-%i" % (chrom.name, phit, hit)
            if frag_size < 203:
                iv = HTSeq.GenomicInterval(chrom.name, phit, hit)
                rrbs_methylome[iv] += frag_id
            else:
                iv = HTSeq.GenomicInterval(chrom.name, phit, (phit+read_len))
                rrbs_methylome[iv] += frag_id+"_left"
                iv = HTSeq.GenomicInterval(chrom.name, hit-read_len, hit)
                rrbs_methylome[iv] += frag_id+"_right"
            out_handle.write("%s\t%i\t%i\n" % (chrom.name, phit, hit))
            phit = hit
        # last fragment per chrom
        frag_size = chrom_len-hit
        frag_id = "%s:%i-%i" % (chrom.name, hit, chrom_len)
        if frag_size < 203:
            iv = HTSeq.GenomicInterval(chrom.name, hit, chrom_len)
            rrbs_methylome[iv] += frag_id
        else:
            iv = HTSeq.GenomicInterval(chrom.name, hit, (hit+read_len))
            rrbs_methylome[iv] += frag_id+"_left"
            iv = HTSeq.GenomicInterval(chrom.name, chrom_len-read_len, chrom_len)
        rrbs_methylome[iv] += frag_id+"_right"
        out_handle.write("%s\t%i\t%i\n" % (chrom.name, hit, chrom_len))


## 2nd read the / a summary file produced by bismark
# Keep in mind:
#  HTSeq is working with 0 based coordinates
#  bismark puts out 1 based coordinates
#  The output from my script have to be 1 based coordinates
    
counts = defaultdict(list)
no_feat = list()
with open(args.input, "r") as in_handle:
    in_handle.next()
    for line in in_handle:
        line = line.strip().split("\t")
        iiv = HTSeq.GenomicInterval(line[1], int(line[2])-1, int(line[2])) # check maybe GenomicPos class
        loc_id = set()
        for iv, val in rrbs_methylome[ iiv ].steps():
            loc_id.update(val)
        if len(loc_id) == 0:
            iid = "%s\t%s\t%s" % (line[1], line[2], line[2])
            no_feat.append(iid) 
        elif len(loc_id) == 1:
            counts[list(val)[0]].append(float(line[3]))  ## add average count 

# Summarise
mean_counts = dict()
for c in counts:
    cc = len(counts[c])
    mean = sum(counts[c])/cc
    mean_counts[c] = (mean, cc)

#no_featO = OrderedDict(sorted(no_feat.items()))
mean_countsO = OrderedDict(sorted(mean_counts.items()))

# write


with open(args.output+".insilico.no_feature.txt", "w") as out_handle:
    for line in no_feat:
        out_handle.write("%s\n" % line)

with open(args.output+".insilico.methylome_summary.txt", "w") as out_handle:
    out_handle.write("%s\t%s\t%s\n" %  ("methylome_id", "mean_coverage", "cpg_count"))
    for line in mean_countsO:
        out_handle.write(line+"\t%.2f\t%.2f\n" %  mean_countsO[line])


